# Screenshots of this Emacs config

The dashboard is opened every time a new window is spawned. You can call it at any time using <kbd>F1</kbd>:

![dashboard](https://codeberg.org/mehrad/.emacs.d/raw/branch/main/misc/screenshots/2025-02-20_12-10-14_dashboard.png)

-------

This config helps you to keep track of what you have done to the file by showing different types of modification in the fringe:

![File modification indicators](https://codeberg.org/mehrad/.emacs.d/raw/branch/main/misc/screenshots/2025-02-20_12-53-28_modification-fringes.png)

-------

Using fuzzy matching can highly boost your productivity by avoiding typing the whole thing:

![cousel fuzzy match](https://codeberg.org/mehrad/.emacs.d/raw/branch/main/misc/screenshots/2025-02-20_12-57-12_vertico.png)

-------

At any given time, you can use C-M-? to see which keybvindings are available to you:

![which-key](https://codeberg.org/mehrad/.emacs.d/raw/branch/main/misc/screenshots/2025-02-20_12-41-18_whichkey.png)

-------

Integrated file manager of Emacs has some better color-coding:

![dierd file manager](https://codeberg.org/mehrad/.emacs.d/raw/branch/main/misc/screenshots/2025-02-20_12-28-04_dired.png)

-------

You can toggle tab bar visibility (C-x t v) and open things in tabs:

![Editor tabs](https://codeberg.org/mehrad/.emacs.d/raw/branch/main/misc/screenshots/2025-02-20_12-47-16_tabs.png)

-------

to quickly switch between files of a project, you can use the treemacs sidebar file manager:

![sidebar file manager](https://codeberg.org/mehrad/.emacs.d/raw/branch/main/misc/screenshots/2025-02-20_12-44-51_treemacs.png)


-------

The default scratch buffer of emacs now uses org-mode since org-mode allows you to run code snippets from different languages within the org file:

![scratch buffer](https://codeberg.org/mehrad/.emacs.d/raw/branch/main/misc/screenshots/2025-02-20_14-24-03_scratch-buffer.png)

The block indicators for org files are customized for better visibility:

![org file](https://codeberg.org/mehrad/.emacs.d/raw/branch/main/misc/screenshots/2025-02-20_14-26-24_org-file.png)

-------

Magit is the git helper interface:

![magit version control manager](https://codeberg.org/mehrad/.emacs.d/raw/branch/main/misc/screenshots/2025-02-20_12-20-06_magit.png)

![magit changes](https://codeberg.org/mehrad/.emacs.d/raw/branch/main/misc/screenshots/2025-02-20_12-18-00_magit-diff.png)


-------

Using the popper package to create popup buffers, and the eat terminal package, you can have a popup fully functional terminal which can even show `htop` or any other ncurses TUI application:

![popper + eat](https://codeberg.org/mehrad/.emacs.d/raw/branch/main/misc/screenshots/2025-02-20_14-35-19_popper-eat.png)

![popper + eat + htop](https://codeberg.org/mehrad/.emacs.d/raw/branch/main/misc/screenshots/2025-02-20_15-06-00-popper-eat-htop.png)
