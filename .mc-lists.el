;; This file is automatically generated by the multiple-cursors extension.
;; It keeps track of your preferences for running commands with multiple cursors.

(setq mc/cmds-to-run-for-all
      '(
        beginning-of-visual-line
        comment-line
        default-indent-new-line
        end-of-visual-line
        ess-insert-assign
        ess-smart-comma
        ess-yank
        forward-sexp
        markdown-end-of-line
        markdown-outdent-or-delete
        mm/delete-region-or-backward-word
        mm/kill-region-or-backward-word
        mm/smart-beginning-of-line
        mm/smart-end-of-line
        my/kill-region-or-backward-word
        my/smart-beginning-of-line
        my/smart-end-of-line
        org-beginning-of-line
        org-end-of-line
        org-self-insert-command
        org-yank
        revert-buffer
        undo-fu-only-undo
        undo-redo
        wdired--self-insert
        ))

(setq mc/cmds-to-run-once
      '(
        View-search-last-regexp-forward
        amx
        counsel-M-x
        dired-toggle-read-only
        drag-stuff-up
        ess-eval-region-or-line-visibly-and-step
        helpful-callable
        ido-find-file
        mm/scroll-down-half-page
        mm/split-window-below-and-follow
        mode-line-other-buffer
        mouse-buffer-menu
        mouse-drag-region-rectangle
        mouse-set-region
        org-cycle-agenda-files
        org-shiftcontrolright
        org-shiftright
        read-only-mode
        reload-user-init-file
        scroll-bar-toolkit-scroll
        swiper-isearch
        undo-redo
        view-emacs-news
        wdired-finish-edit
        which-key-C-h-dispatch
        windresize
        windresize-down
        windresize-down
        windresize-left
        windresize-right
        windresize-up
        yank-rectangle
        ))
