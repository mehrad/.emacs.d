(org-babel-load-file
 (expand-file-name
  "config.org"
  user-emacs-directory))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(inhibit-startup-screen t)
 '(package-selected-packages
   '(erc-view-log marginalia wc-mode crdt lua-mode yaml-mode vertico gcmh use-package))
 '(safe-local-variable-values
   '((geiser-repl-per-project-p . t)
     (eval with-eval-after-load 'yasnippet
           (let
               ((guix-yasnippets
                 (expand-file-name "etc/snippets/yas"
                                   (locate-dominating-file default-directory ".dir-locals.el"))))
             (unless
                 (member guix-yasnippets yas-snippet-dirs)
               (add-to-list 'yas-snippet-dirs guix-yasnippets)
               (yas-reload-all))))
     (eval setq-local guix-directory
           (locate-dominating-file default-directory ".dir-locals.el"))
     (eval add-to-list 'completion-ignored-extensions ".go"))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(highlight-changes ((t (:foreground unspecified :background "#1a3717" :underline unspecified))))
 '(highlight-changes-delete ((t (:foreground unspecified :background "#361b17" :underline unspecified))))
 '(tab-bar ((t (:background "gray8" :extend t))))
 '(tab-bar-tab ((t (:foreground "salmon" :background "#252621" :overline t :slant normal))))
 '(tab-bar-tab-inactive ((t (:foreground "#864841" :slant italic)))))
(put 'narrow-to-region 'disabled nil)
