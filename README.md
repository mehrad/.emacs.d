# .emacs.d

This is my attempt to put together my own emacs config. If you want to see the config, checkout the [config.org](https://codeberg.org/mehrad/.emacs.d/src/branch/main/config.org) file for the full detailed info.

![Screenshot of the dashboard of my config](https://codeberg.org/mehrad/.emacs.d/raw/branch/main/misc/screenshots/2025-02-20_12-10-14_dashboard.png)

## Table Of Content

- [Main Idea](#main-idea)
  - [Screenshots](#screenshots)
- [Beginner's Guide](#beginner-s-guide)
    - [Installation](#installation)
    - [Usage](#usage)
    - [Some beginner Emacs terminology](#some-beginner-emacs-terminology)
    - [Basic Emacs key bindings](#basic-emacs-key-bindings)
    - [Using mouse](#using-mouse)
- [non-Default Keybindings](#non-default-keybindings)
- [Keybindings in major modes](#keybindings-in-major-modes)
    - [Dashboard](#dashboard)
    - [dired](#dired)
    - [Magit](#magit)
    - [ESS](#ess)
- [Keybindings in minor modes](#keybindings-in-minor-modes)
    - [bm](#bm)
    - [hl-todo](#hl-todo)
- [Custom functions](#custom-functions)
- [Private config](#private-config)
- [Improving the loading speed](#improving-the-loading-speed)
- [Troubleshooting](#troubleshooting)


-------------------------------------------------

## Main Idea

The general idea of this config is to be my personal config, so everything is to my liking, but there are clear instructions for you to change it also to your liking. :)

In general I've tried to keep the default key bindings as the more you change stuff, the chance of collision is increases. But that said, some of the Emacs keybindings are insane and therefore I have changed them.

In the following, `C` means <kbd>Ctrl</kbd> and `S` means <kbd>Shift</kbd>, `M` means <kbd>Alt</kbd> keys, `RET` or `<return>` means the <kbd>Enter</kbd>. The "region" means the selected test, and the "killring" refers to the Emacs killring which is something like a clipboard that when you copy ("yank"), or cut ("kill") a text, it will store the data in there.

I have tested the loading time of this config using [hyperfine](https://github.com/sharkdp/hyperfine) to show that it actually loads pretty fast. The following is when running it on the commit [951c8e9](https://codeberg.org/mehrad/.emacs.d/commit/951c8e9e8011d5929880828063eaceb921e8306d):

- running the daemon (the way I suggest using Emacs):
    ```console
    ❯  hyperfine 'emacs --fg-daemon --kill'
    Benchmark 1: emacs --fg-daemon --kill
      Time (mean ± σ):      2.884 s ±  0.057 s    [User: 2.122 s, System: 0.428 s]
      Range (min … max):    2.794 s …  2.957 s    10 runs
    ```

- running emacs without daemon:
    ```console
    ❯  hyperfine 'emacs --kill'
    Benchmark 1: emacs --kill
      Time (mean ± σ):      2.995 s ±  0.063 s    [User: 2.191 s, System: 0.412 s]
      Range (min … max):    2.917 s …  3.085 s    10 runs
    ```

### Screenshots

To see the screenshot gallery, [**click here**](https://codeberg.org/mehrad/.emacs.d/src/branch/main/misc/screenshots.md).

-------------------------------------------------

## Beginner's Guide

### Installation

This is an Emacs config, therefore there is no such thing as installation.
All you have to do is to do the following in this order:
1. Install Emacs (on Arch-based distros you can run `sudo pacman -S emacs`)
2. Download/Clone this repository and place it in your home (`~/`) folder, so that the `config.org` file be at `~/.emacs.d/config.org`. The easiest way is to do `git clone --depth 1 https://codeberg.org/mehrad/.emacs.d.git ~/` which does a shallow clone of this git repository
3. The first run is the longest one because Emacs should install all the packages that we have instructed it in the `config.org` file. So it is a good idea to run emacs graphical user interface but from terminal so see every message you have to see: `emacs`. There might be few packages that would need your permission to get compiled (e.g `vterm`), so be ready to type `y` when needed
4. when everything is installed and loaded without errors, which most probably is the case, close Emacs by <kbd>Ctrl</kbd><kbd>x</kbd> and then <kbd>Ctrl</kbd><kbd>c</kbd>, and then restart it again just to make sure everything is fully installed and loaded (you should see your Emacs look very similar to the picture above)

### Usage

For using Emacs in general, I suggest you to run Emacs as a daemon and every time you need an Emacs, just spawn a client.
To do this, you can run the daemon with `emacs --bg-daemon`, and to spawn the client you can use either of:
- `emacsclient --create-frame` to open GUI (Graphical User Interface)
- `emacsclient --tty` to open emacs in terminal or TTY
Also note that because you have used daemon, all these clients can access each other's buffers (i.e open files and etc). This is good because you can easily manage all your files from the same frame (the Emacs terminology that refers to a graphical window) you want, or from multiple frames.

I strongly recommend you to read the [`config.org`](https://codeberg.org/mehrad/.emacs.d/src/branch/main/config.org) as I have explained everything there.
All your questions about this config can be easily answered there.

But to help running Emacs daemon and `emacsclient` efficiently and easily, I added a script (add in 7eb85da). So **the easiest way to run Emacs** is to bind a keybinding in your Desktop Environment or Window Manager to run `bash ~/.emacs.d/invoke-emacsclient.sh`.

### Some beginner Emacs terminology

- **Frame:** The window that your window-manager or Desktop environment is handling. It is the graphical window you see and you can move with your mouse and can minimize/maximize/resize
- **Window:** The visible area in the _frame_ that shows some content (e.g content of a file, information, user manual). They cannot be minimized or get hidden. it is your window through which you see the content.
- **Buffer:** The thing that contains the content that is shown in a _window_.

To be more clear, an Emacs _window_ is just a place to display a _buffer_. You as the user can choose which _buffer_ to show in which _window_ or which _frame_. As an example, you can have one _frame_ which contains 3 _windows_ and you can have 2 files opened (which means 2 _buffers_), and you have the _buffer_ of file1 showing in the top-left _window_ and at the same time right _window_, and _buffer_ of file2 in the bottom-left _window_:

```
┏━━━━━━━━━━━━━━━━━Frame━━━━━━━━━━━━━━━┓
┃╔═════Window1══════╗╔════Window2════╗┃
┃║┌┄┄┄┄Buffer1┄┄┄┄┄┐║║┌┄┄┄Buffer1┄┄┄┐║┃
┃║┆                ┆║║┆             ┆║┃
┃║┆     file1      ┆║║┆             ┆║┃
┃║┆                ┆║║┆             ┆║┃
┃║┆                ┆║║┆             ┆║┃
┃║└┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┘║║┆             ┆║┃
┃╚══════════════════╝║┆   file1     ┆║┃
┃╔═════Window3══════╗║┆             ┆║┃
┃║┌┄┄┄┄Buffer2┄┄┄┄┄┐║║┆             ┆║┃
┃║┆                ┆║║┆             ┆║┃
┃║┆     file2      ┆║║┆             ┆║┃
┃║┆                ┆║║┆             ┆║┃
┃║└┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┘║║└┄┄┄┄┄┄┄┄┄┄┄┄┄┘║┃
┃╚══════════════════╝╚═══════════════╝┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
```

**Note** that the _buffer1_ which contains _file1_ is viewed in two different _windows_ and each _window_ can show different parts of the _buffer1_.

### Basic Emacs key bindings

These are a very small portion of the default keybindings in Emacs. I have put them here for users who are new to Emacs.

| Keybinding | Action                                              |
|:-----------|:----------------------------------------------------|
| `C-x C-f`  | Open/find file                                      |
| `C-x C-s`  | Save current file                                   |
| `C-x k`    | Close current buffer/file                           |
| `M-%`      | Find and replace one-by-one                         |
| `C-x C-d`  | Open Folder/directory                               |
| `C-g`      | Cancel any operation                                |
| `C-w`      | Cut selected region, or cut one word backwards      |
| `M-w`      | Copy the selected region                            |
| `C-y`      | Yank (a.k.a paste) from kill-ring (a.k.a clipboard) |
| `C-x 2`    | Split vertically (up and down)                      |
| `C-x 3`    | Split horizontally (left and right)                 |

To get help from Emacs (yes, it can give you very good and compact info), use the following keybindings:

| Keybinding | Action                                                   |
|:-----------|:---------------------------------------------------------|
| `C-h C-h`  | Open different options for different helps               |
| `C-h k`    | Asks for a key press and shows what that keybinding does |
| `C-h f`    | Asks for a function name and opens its description       |
| `C-h v`    | Asks for a variable name and opens its description       |
| `C-h m`    | Shows all possible keybinding in the buffer you are on   |

### Using mouse

- You can access Global Menu by holding <kbd>Ctrl</kbd> and right-clicking. This menu let you find the settings you want in a GUI way and also shows you the related keybindings
- To open Buffer Menu (list of open buffers and files), hold <kbd>Ctrl</kbd> and left-click.
- To select a region, either left-click and drag, or a better way is to left-click to mark the beginning, and right-click to mark the end of a region.

**Note:** you can close any of these menus by pressing `C-g` or clicking outside of the menu.

-------------------------------------------------

## non-Default Keybindings

Other than the following, everything else is the default

| Keybinding                | Action                                                          |
|:--------------------------|:----------------------------------------------------------------|
| `C-h b`                   | Fuzzy search into keybindings                                   |
| <kbd>F1</kbd>             | Open dashboard in the current buffer                            |
| <kbd>F5</kbd>             | Load the current file from disk (in case it is changed)         |
| `C-+` or `C-<wheel-up>`   | Increase font size                                              |
| `C--` or `C-<wheel-down>` | Decrease font size                                              |
| `C-0`                     | Reset the font size scaling                                     |
| `C-S-p` / `C-S-n`         | Move up/down 8x faster                                          |
| `C-S-M-p` / `C-S-M-n`     | Scroll half page up /down                                       |
| `C-a` / `C-e`             | Smart home/end (respecting leading space and inline comment)    |
| `C-S-v`                   | Yank (a.k.a paste)                                              |
| `C-S-s`                   | Find all instances of the selected text or the string at point  |
| `C-d`                     | Delete whole line or selected lines                             |
| `C-w`                     | Kill a word or region backwards                                 |
| `M-<backspace>`           | Delete the word or region backwards without copying to killring |
| `C-DEL`                   | Kill a word forward                                             |
| `M-DEL`                   | Delete a word forward                                           |
| `C-x [arrows]`            | Jump to the window in that direction                            |
| `C-x S-o`                 | Jump to previous window (reverse of `C-x o`                     |
| `C-x C-r`                 | Recent files with Ivy                                           |
| `C-x C-S-r`               | List of recent files in a buffer                                |
| `C-z` / `C-S-z`           | Undo/Redo                                                       |
| `C-x C-b`                 | Switch buffers in Ibuffer                                       |
| `C-x b`                   | Switch buffer with Ivy                                          |
| `C-x C-,`                 | Switch to previous buffer                                       |
| `C-S-c C-S-c`             | Activate multi cursor mode in the current region                |
| `C-S-<left click>`        | Add an additional cursor at mouse click location                |
| `M-S-n` / `M-S-p`         | Select the previous/next occurrence of the selected region      |
| `C-S-c C-s`               | Select all the occurrences of the selected region               |
| `M-↑` / `M-↓`             | Move a line or region content up/down                           |
| `M-p` / `M-n`             | Move a line or region content up/down                           |
| `C-<tab>`                 | Toggle code folding                                             |
| `C-S-<tab>`               | Fold children sections                                          |
| `C-c M-n`                 | Duplicates line or region right after the line or region        |
| `M-;`                     | Toggle comment of the line or region                            |
| `C-x w`                   | Toggle window resize minor mode (RET to accept changes)         |
| <kbd>Esc</kbd>            | Works similar to what the default `C-g` does                    |
| <kbd>Scroll Lock</kbd>    | Toggle the read-only mode for the current buffer                |
| `C-M-S-<return>`          | Open eat terminal emulator                                      |
| `C-x C-[left/right]`      | Switch to next or previous buffer                               |
| `C-x t v`                 | Toggle showing the tab-bar                                      |
| `C-x t s`                 | Switch to tab                                                   |
| `C-x t [1-4]`             | Switch to tab numbers 1 to 4                                    |
| `C-x t +`                 | Open new blank tab                                              |
| `C-x t k`                 | Kill the buffer, close the tab, and hide the tab-bar            |
| `C-.`                     | Triggers code completion suggestion                             |
| `C-h C-.`                 | Get help for the symbol/text that in under the point            |
| `C-x C-p 2`               | Create a perpective (something like workspace)                  |


You can press `C-h` or `C-?` at any point while you are in the middle of a keybinding chain to see the possible options. For example try `C-x C-h`.
As an alternative, in any buffer you can do `C-h b` to show all the keybindings available for that buffer. Also because we use Counsel, in this config, you get to easily fuzzy search for the functionality you want rather than reading and scrolling the list.

-------------------------------------------------

## Keybindings in major modes

You can always use <kbd>?</kbd> to get the keybindings for all these major modes unless stated otherwise.
Also as a general rule in Emacs, you can use `C-h m` in any active buffer to get the active minor modes and all active keybindings. To facilitate finding the keybindings, we use the `which-key` package which can be used in any major mode by `C-M-?` (which is equivalent of `M-x which-key-show-major-mode`) or for even more extensive list you can `M-x which-key-show-full-major-mode`.

In the following we just list some of the keybindings to give you a head-start:


### Dashboard

The following are the essential defaults (for more use <kbd>?</kbd>):

| Keybinding                               | Action                           |
|:-----------------------------------------|:---------------------------------|
| <kbd>F1</kbd>                            | Load the dashboard from anywhere |
| <kbd>r</kbd>                             | Jump to "Recent Files"           |
| <kbd>p</kbd>                             | Jump to "Projects"               |
| `C-n`/`C-p` or <kbd>↑</kbd>/<kbd>↓</kbd> | Go to next/previous item         |


Custom keybinding:

| Keybinding      | Action                        |
|:----------------|:------------------------------|
| `C-S-n`/`C-S-p` | Jump to next/previous section |


### dired

Dired is the file manager in Emacs and you can start it with `C-x d`. You can get a very brief and quick help by pressing <kbd>?</kbd> in dired. List of all keybindings can be accessed via `M-x describe-bindings`.

| Keybinding                   | Action                                                 |
|:-----------------------------|:-------------------------------------------------------|
| <kbd>j</kbd>                 | Jump to file using fuzzy finder                        |
| <kbd>o</kbd>                 | Open file in a new window                              |
| <kbd>a</kbd>                 | Open the directory in the same buffer                  |
| <kbd>i</kbd>                 | Insert the content of directory in the same buffer     |
| `C-_`                        | Undo (this would also undo <kbd>i</kbd>                |
| <kbd>+</kbd>                 | Create a new directory                                 |
| <kbd>Shift</kbd><kbd>d</kbd> | Delete the current file/folder now                     |
| <kbd>d</kbd>                 | Mark the current file/folder to be deleted later       |
| <kbd>x</kbd>                 | Apply the deletion based on deletion marks             |
| <kbd>m</kbd>                 | Mark the current file/folder                           |
| <kbd>u</kbd>                 | Unmark the current file/folder                         |
| <kbd>Shift</kbd><kbd>u</kbd> | Unmark all the marks                                   |
| <kbd>c</kbd>                 | Compress                                               |
| <kbd>Shift</kbd><kbd>c</kbd> | Copy                                                   |



Custom keybindings:

| Keybinding                      | Action                                                 |
|:--------------------------------|:-------------------------------------------------------|
| double left-click               | Open the file in the current window                    |
| <kbd>Ctrl</kbd><kbd>Enter</kbd> | Open in other window                                   |
| <kbd>Ctrl</kbd>left-click       | Mark the file                                          |
| <kbd>e</kbd>                    | diff the two marked files                              |
| `C-c C-s`                       | Find files in the folder that match a globbing pattern |
| <kbd>z</kbd>                    | Opens filter to easily narrow down the file list       |


### Magit

The following are the essential defaults (for more use <kbd>?</kbd>):

| Keybinding                                                            | Action                    |
|:----------------------------------------------------------------------|:--------------------------|
| <kbd>n</kbd>/<kbd>p</kbd> or `C-n`/`C-p` or <kbd>↑</kbd>/<kbd>↓</kbd> | Go to next/previous item  |
| <kbd>F</kbd>/<kbd>P</kbd>                                             | Git Pull/Push             |
| <kbd>z</kbd>                                                          | Stash                     |
| <kbd>s</kbd>/<kbd>u</kbd>                                             | Stage/unstage             |
| <kbd>Tab</kbd>                                                        | Collapse or open sections |


### ESS

The following are the essential defaults

| Keybinding   | Action                                          |
|:-------------|-------------------------------------------------|
| `C-c C-j`    | Evaluate line                                   |
| `C-c C-b`    | Evaluate buffer                                 |
| `C-<return>` | Send line or region to REPL and step            |
| `C-c C-c`    | Evaluate region, function or paragraph and step |

You can run R REPL with `M-x run-ess-r`.

Custom keybindings:

| Keybinding | Action                               |
|:-----------|:-------------------------------------|
| `M--`      | Insert `<-` (assign) at the point    |
| `M-,`      | Insert `|>` (base pipe) at the point |
| `C-c l`    | Clear R console                      |

### Perspective

This mode allows you create workspaces so that you can easily work with two projects using the same Emacs server and your buffer list does not mix and you can stay focus on each project. Perspective mode is loaded by default in this config, and by default you will be in the ~[main]~ perspective which is also indicated in the modeline in blue. The following are the keybindings you might need to start enjoying this mode.

| Keybinding    | Action                                                   |
|:--------------|:---------------------------------------------------------|
| `C-x C-p 2`   | Create a new perspective with the name you like          |
| `C-x C-p s`   | Switch between perspectives                              |
| `C-x C-p c`   | Kills the perspective you are in                         |
| `C-x C-p C-b` | ibuffer in this perpective and hiding other perspectives |

As always, because of the fabilous `whichkey` mode, you can add `?` in the key binding sequence to get the full list of bindings (i.e. `C-x C-p ?`).

### treemacs

The treemacs creates a sidebar on the left side of the window to show for example the files we have in the project. You can toggle it by pressing `C-x d t`. The following are the keybindings we have defined:

In buffers other than treemacs:

| Keybindings | Action                                      |
|:------------|:--------------------------------------------|
| `C-x d t`   | Toggle the sidebar                          |
| `C-x d d`   | Select a folder to be displayed in treemacs |
| `C-x d b`   | Toggle treemacs bookmark                    |
| `M-0`       | Focus the treemacs                          |


In treemacs buffer:

| Keybindings      | Action              |
|:-----------------|:--------------------|
| <kbd>Space</kbd> | toggle file preview |


-------------------------------------------------

## Keybindings in minor modes

**What is minor mode:** In Emacs you typically have one active major mode which defines the major functionality you are interacting with (e.g showing PDF, syntax highlighting code, running the code, and etc.), and then you can have multiple minor modes which spice up the functionality and add extra features (e.g showing spelling mistakes, bookmark some lines, showing extra help, and etc.).

In this section I list some of the keybindings for the minor modes that a typical user might interact with many times a day.

### bm

It is very useful to bookmark certain lines so that you can get back to those lines while working on the text/code. Emacs provides bookmarking functionality, but you need to give them names and which make working with them a bit slow although it is good if you have many bookmarks. The `bm` minor mode allow you to quickly bookmark a line and then quickly go to next or previous. This mode does not have any default bindings, so everything below is custom bindings I have defined:

| Keybinding | Action                            |
|:-----------|:----------------------------------|
| `C-c b t`  | Toggles the line being a bookmark |
| `C-c b p`  | Jump to previous bookmark         |
| `C-c b n`  | Jump to next bookmark             |
| `C-c b l`  | List all the bookmarks            |

You can also toggle the bookmark by clicking on the left margin and left fringe (basically click somewhere between the left side of line number and the edge of the window).


### hl-todo

The `hl-todo` minor mode change the font color of some texts (e.g `TODO`, `FIXME`) in the comments. This is very useful in programming to be able to quickly spot the TODOs and issues you have marked. This minor mode also allow you to jump between these markings. This mode does not have any default bindings, so everything below is custom bindings I have defined:

| Keybinding | Action           |
|:-----------|:-----------------|
| `C-c t p`  | Jump to previous |
| `C-c t n`  | Jump to next     |


-------------------------------------------------

## Custom functions

As per convention, all functions written in this config are prepended with `my/`, therefore, typing `M-x my/` and then pressing <kbd>Tab</kbd> can list them. The names are pretty self-explanatory in my opinion.

-------------------------------------------------

## Private config

Emacs is pretty extensible, and there are times that you would like to avoid committing a setting to your config git repo, either to keep them on one computer (e.g personal home computer stuff from work computer), or to have your main config as a public repo and still keep some config privately in another repo.

In this config and at the very end, we also load any `.org` file that is in the `private-config/` directory (typically located in `~/.emacs.d/`). So you can create that folder and put your private org-mode config files there to be loaded at the end.

This is also a very good place to test things that you want to add to your config. At least this is what I generally do. I create a file for that particular package or configuration, gradually work on it and test it, but whenever I feel I want to turn it off, I can just exclude that `.org` file and be sure that the rest of my config is unaffected and works perfectly.

-------------------------------------------------

## Improving the loading speed

I highly encourage you to use the daemon-client paradigm of Emacs as you load all the config and packages into memory once (when starting the daemon), and after that loading the `emacsclient` would be instantaneous. To help you with this process, you can use the `invoke-emacsclient.sh` (instructions can be found in the first we lines of `invoke-emacsclient.sh`.

But this does not mean that you should foret about the loading speed of the Emacs. By optimizing your config, you can drastically improve the loading speed and reduce the loading time. In this config I usually do my best to keep things light and snappy, but you should of course explore this and improve your local copy if you wish so :)

To do this, please refer to the [profiling-the-config.org](https://codeberg.org/mehrad/.emacs.d/src/branch/main/profiling-the-config.org) file.

-------------------------------------------------

## Troubleshooting

From time to time some packages get updated and the Emacs complains about issues in accessing some `.tar` file of some packages. It would be enough to run `M-x package-refresh-contents` to update the packages and then `M-x reload-user-init-file` to reload the config.

It also happens (rarely) that a package gets updated but the dependencies are not compiled or refreshed ([see this example](https://github.com/magit/magit/issues/4841)), In this situation the best thing to do is to force Emacs to recompile the packages via `M-x byte-force-recompile RET ~/.emacs.d/ RET`. This would take a minute or two, but it does work.

Perhaps these can also be automated through a script or function, but well, it is not a frequent issue and therefore not a priority.
