#!/bin/env bash

################################################################################
## Description:                                                               ##
##   The idea of this script is to effortlessly invoke and run emacsclient.   ##
##   Meaning it would run emacs server after making sure emacs is running as  ##
##   a daemon.                                                                ##
##                                                                            ##
## Usage:                                                                     ##
##   Running it without any argument would result in opening emacsclient in   ##
##   GUI mode, but this can be controlled by providing 'gui' or 'tty' as the  ##
##   first argument. you can also omit this and it will fallback on 'gui'.    ##
##   and at the end you can list the files you want emacs to open.            ##
##                                                                            ##
## Examples:                                                                  ##
##   # to run in GUI mode                                                     ##
##   bash ~/.emacs.d/invoke-emacsclient.sh gui                                ##
##                                                                            ##
##   # to run in terminal or tty                                              ##
##   bash ~/.emacs.d/invoke-emacsclient.sh tty                                ##
##                                                                            ##
##   # to open files                                                          ##
##   bash ~/.emacs.d/invoke-emacsclient.sh file.txt                           ##
##   bash ~/.emacs.d/invoke-emacsclient.sh gui file.txt                       ##
##   bash ~/.emacs.d/invoke-emacsclient.sh tty file.txt                       ##
################################################################################
## Author:                                                                    ##
##   Mehrad Mahmoudian (mehrad.ai)                                            ##
## License:                                                                   ##
##   GPLv3                                                                    ##
################################################################################


## make sure emacs is installed
if [ ! "$(command -v emacs)" ]; then
    echo 'It seems Emacs is not installed!'
    exit 1
fi


# if no argument was provided
if [ $# -eq 0 ]; then
    TYPE="gui"
    FILES=""
else
    if [ "${1}" = "gui" ] || [ "${1}" = "tty" ] || [ "${1}" = "help" ]; then
        TYPE="${1}"
        shift 1
        FILES="${@}"
    else
        TYPE="gui"
        FILES="${@}"
    fi
fi


function func_manual() {
    echo 'Description: '
    echo '  This is a script to invoke an instance of emacs as daemon and then it will try'
    echo '   to launch the emacs-client to connect to that daemon.'
    echo
    echo 'Usage:'
    echo '  invoke-emacsclient.sh [subcommand] {files}'
    echo
    echo '  You can choose from one of the following subcommands, but if no subcommand is'
    echo '   specified, it will default to the gui:'
    echo '     gui    To start the client in GUI mode'
    echo '     tty    To start the client in TTY mode'
    echo '     help   To show this help message'
	echo
    echo 'Git repo:'
    echo '  https://codeberg.org/mehrad/.emacs.d'
    echo

    exit 0
}


function func_start_daemon() {
    # if emacs daemon is NOT running
    if ! pgrep -f 'emacs --bg-daemon' > /dev/null; then
        notify-send --app-name='invoke-emacsclient.sh' \
                    --urgency='low' \
                    'Starting Emacs Daemon...'
        
        # start the ssh-agent daemon and get the environmental variables so that emacs inherit them
        # note that if you run emacs without this script, your ssh-agent will not be exposed to emacs
        eval "$(ssh-agent -s)"
        
        # run the emacs daemon in background
        if ! emacs --bg-daemon; then
            notify-send --app-name='invoke-emacsclient.sh' \
                        --urgency='critical' \
                        '❌ Emacs daemon failed to start!'
        fi
        
        # if user have provided a file, wait for half a second to allow daemon to settle
        if [ "${#FILES}" -gt 0 ]; then
            sleep 0.5s
        fi
    fi
}



# start the daemon
func_start_daemon


# the command that will be run to invoke emacs process
CMD="emacsclient --alternate-editor=''"


# if emacs daemon is running
if pgrep -f 'emacs --bg-daemon' > /dev/null; then
    case "${TYPE}" in
        help|-h|--help)
            func_manual;
            ;;
        tty)
            CMD="${CMD} --tty";
            ;;
    	gui)
	        CMD="${CMD} --create-frame --no-wait";
	        ;;
        *)
            func_manual;
            ;;
    esac

    # if user have specifid some files to be opened
    if [ "${#FILES}" -gt 0 ]; then
        CMD="${CMD} \"${FILES}\""
    fi

    eval "${CMD}"
else
    notify-send --app-name='invoke-emacsclient.sh' \
                --urgency='critical' \
                '❌ Apparently running emacs has lead to some error. Investigate!'
    exit 1
fi
